package Colégiosistemadecadastro;

public class Diretores extends Funcionarios {
    private double ParticipaçaoLucros;


    public Diretores(String nome, String matricula, String cpf, String rg, String endereço, String cargo, double salario, double ParticipaçaoLucros) {
        super(nome, matricula, cpf, rg, endereço, cargo, salario);
        this.ParticipaçaoLucros = ParticipaçaoLucros;
    }

    public double getParticipaçaoLucros() {
        return ParticipaçaoLucros;
    }

    public void setParticipaçaoLucros(double participaçaoLucros) {
        ParticipaçaoLucros = participaçaoLucros;
    }

    public void relatorioPagamentoDiretor(Diretores diretores){
        System.out.println("Nome: " + diretores.getNome());
        System.out.println("Matricula: " + diretores.getMatricula());
        System.out.println("CPF:" + diretores.getCpf());
        System.out.println("Endereço: " + diretores.getEndereço());
        System.out.println("RG: " + diretores.getRg());
        System.out.println("Cargo: " +diretores.getCargo());
        System.out.println("Salario: " + diretores.getSalario());
        System.out.println("PNL: " + diretores.getParticipaçaoLucros());
        Salario = ParticipaçaoLucros + Salario;
        System.out.println("Total Salário: " + Salario );

        diretores.calcularPagamento();
    }

    @Override
    public void calcularPagamento() {
        this.Salario = this.Salario + ParticipaçaoLucros;
    }
}
