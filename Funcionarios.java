package Colégiosistemadecadastro;

public abstract class  Funcionarios {
    private String nome;
    private String matricula;
    private String cpf;
    private String Rg;
    private String endereço;
    private String Cargo;
    protected  double Salario;

    @Override
    public String toString() {
        return "Funcionarios{" +
                "nome='" + nome + '\'' +
                ", matricula='" + matricula + '\'' +
                ", cpf='" + cpf + '\'' +
                ", Rg='" + Rg + '\'' +
                ", endereço='" + endereço + '\'' +
                ", Cargo='" + Cargo + '\'' +
                ", Salario=" + Salario +
                '}';
    }

    public Funcionarios() {

    }

    public Funcionarios(String nome, String matricula, String cpf, String rg, String endereço,String cargo, double salario) {
        this.nome = nome;
        this.matricula = matricula;
        this.cpf = cpf;
        this.Rg = rg;
        this.endereço = endereço;
        this.Cargo = cargo;
        this.Salario = salario;
    }
    public abstract void calcularPagamento();

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return Rg;
    }

    public void setRg(String rg) {
        Rg = rg;
    }

    public String getEndereço() {
        return endereço;
    }

    public void setEndereço(String endereço) {
        this.endereço = endereço;
    }

    public String getCargo() {
        return Cargo;
    }

    public void setCargo(String cargo) {
        Cargo = cargo;
    }
    public double getSalario() {
        return Salario;
    }

    public void setSalario(double salario) {
        Salario = salario;
    }
}
