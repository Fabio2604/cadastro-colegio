package Colégiosistemadecadastro;

public class Professores extends Funcionarios {
    private double HorasExtras;

    public Professores(String nome, String matricula, String cpf, String rg, String endereço, String cargo, double salario, double HorasExtras) {
        super(nome, matricula, cpf, rg, endereço, cargo, salario);
        this.HorasExtras = HorasExtras;
    }
    public double getHorasExtras() {
        return HorasExtras;
    }

    public void setHorasExtras() {
        HorasExtras = HorasExtras;
    }
    public void relatorioPagamentoProfessor(Professores professores){
        System.out.println("Nome: " + professores.getNome());
        System.out.println("Matricula: " + professores.getMatricula());
        System.out.println("CPF: " + professores.getCpf());
        System.out.println("Endereço: " + professores.getEndereço());
        System.out.println("RG:" + professores.getRg());
        System.out.println("Cargo: " + professores.getCargo());
        System.out.println("Salario: " + professores.getSalario());
        Salario = HorasExtras * 50 + Salario;
        System.out.println("Horas Extras (R$50,00/h): " + professores.getHorasExtras()+" Horas extras");
        System.out.println("Total Salário: " + Salario );


        professores.calcularPagamento();
    }

    @Override
    public void calcularPagamento() {
        this.Salario = this.Salario +(HorasExtras * 0.5);
    }
}
