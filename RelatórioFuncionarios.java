package Colégiosistemadecadastro;

import org.w3c.dom.ls.LSOutput;

public class RelatorioFuncionarios {
    public static void main(String[] args) {
       Diretores diretor1 = new Diretores(": Marcelo Santos Silva", ": 3574518D", ": 115.456.121-87", ": 23.745.235-5 RJ",": Rua das Palmeireas 435", "Diretor chefe" ,8500.00 ,20250.00  );
       Diretores diretor2 = new Diretores("Fabrício Mello", "35474214D", "112.256.828-21", "12.421.451-3 RJ","Rua das Flores 651", "Diretor adjunto" ,8500.00,12500  );
       Professores professor1 = new Professores("Maicon Fernandes", "451251351", "434.287.357-45", "55.321.234-4","Rua Do Engenho 123", "Professor de Português",4000.00, 9   );
       Professores professor2 = new Professores("Luis Frederico Castro", "841361587P", "818.642.341-60", "31.345.123-9","Rua Almeida  111 AP 541", "Professor de Matemática",4000.00, 8   );
       Professores professor3 = new Professores("Henrique Albuquerque Medeiros", "987001456P", "555.477.835.21", "98.646.045-20","Rua Alberto Goulart 589", "Professor de História",4000.00,14  );
       Professores professor4 = new Professores("Fabio Santana Junior", "014394419P", "456.001.007-14", "24.795.234-4","AV Marcelo Bastos 456", "Professor de Geografia",4000,20  );
       System.out.println("CADASTRO FUNCIONÁRIOS ");
       System.out.println("----------------------------");
       diretor1.relatorioPagamentoDiretor(diretor1);
       System.out.println("----------------------------");
       diretor2.relatorioPagamentoDiretor(diretor2);
       System.out.println("----------------------------");
       professor1.relatorioPagamentoProfessor(professor1);
       System.out.println("----------------------------");
       professor2.relatorioPagamentoProfessor(professor2);
       System.out.println("----------------------------");
       professor3.relatorioPagamentoProfessor(professor3);
       System.out.println("----------------------------");
       professor4.relatorioPagamentoProfessor(professor4);


    }


}


SAÍDA ---------------------------

CADASTRO FUNCIONÁRIOS 
----------------------------
Nome: : Marcelo Santos Silva
Matricula: : 3574518D
CPF:: 115.456.121-87
Endereço: : Rua das Palmeireas 435
RG: : 23.745.235-5 RJ
Cargo: Diretor chefe
Salario: 8500.0
PNL: 20250.0
Total Salário: 28750.0
----------------------------
Nome: Fabrício Mello
Matricula: 35474214D
CPF:112.256.828-21
Endereço: Rua das Flores 651
RG: 12.421.451-3 RJ
Cargo: Diretor adjunto
Salario: 8500.0
PNL: 12500.0
Total Salário: 21000.0
----------------------------
Nome: Maicon Fernandes
Matricula: 451251351
CPF: 434.287.357-45
Endereço: Rua Do Engenho 123
RG:55.321.234-4
Cargo: Professor de Português
Salario: 4000.0
Horas Extras (R$50,00/h): 9.0 Horas extras
Total Salário: 4450.0
----------------------------
Nome: Luis Frederico Castro
Matricula: 841361587P
CPF: 818.642.341-60
Endereço: Rua Almeida  111 AP 541
RG:31.345.123-9
Cargo: Professor de Matemática
Salario: 4000.0
Horas Extras (R$50,00/h): 8.0 Horas extras
Total Salário: 4400.0
----------------------------
Nome: Henrique Albuquerque Medeiros
Matricula: 987001456P
CPF: 555.477.835.21
Endereço: Rua Alberto Goulart 589
RG:98.646.045-20
Cargo: Professor de História
Salario: 4000.0
Horas Extras (R$50,00/h): 14.0 Horas extras
Total Salário: 4700.0
----------------------------
Nome: Fabio Santana Junior
Matricula: 014394419P
CPF: 456.001.007-14
Endereço: AV Marcelo Bastos 456
RG:24.795.234-4
Cargo: Professor de Geografia
Salario: 4000.0
Horas Extras (R$50,00/h): 20.0 Horas extras
Total Salário: 5000.0
